# Spike Report

## Spike 3 - Blueprint Basics

### Introduction

The purpose of this spike is to teach us the basics of using blueprints in Unreal Engine and how they function.

### Goals

The goal of this spike was to in a new Blueprint FPS Unreal Engine project, create a “Launch-pad” actor, which can be placed to cause the player to leap into the air upon stepping on it.

* Play a sound when the character is launched
* Set the Launch Velocity using a variable, which lets you place multiple launch-pads with different velocities in the same level
* Add an Arrow Component, and use its rotation to define the direction to launch the character
* Create a new level using the default elements available to you from the Unreal Engine (boxes should suffice), which should be a fun little FPS level with many launch-pads that help you access different areas.


### Personnel

* Primary - Mitchell Bailey
* Secondary - N/A

### Technologies, Tools, and Resources used

* [The Quick Start Guide](https://docs.unrealengine.com/latest/INT/Engine/Blueprints/QuickStart/index.html)

### Tasks Undertaken

1. Created launchpad blueprint
1. Coded the launchpad's collision box to recognise when overlapping actor is the player and use the launch character node
1. Created float variable called "Launch Velocity" and linked it to the launch character vector
1. Created a new arrow component with particle system and linked it to the launch character vector
1. Created basic level that is navigatable by using launchpads

### What we found out

* How to use Unreal blueprints to a basic level
* The possibilities to using blueprints